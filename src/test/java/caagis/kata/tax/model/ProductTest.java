package caagis.kata.tax.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class ProductTest {

    @Test
    public void should_return_livre_product_same_price_when_getWithTaxePrice_and_getCalculatedTax_from_no_imported_livre_product() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.LIVRE)
                .imported(false)
                .price(new BigDecimal("12.49"))
                .build();

        // When
        BigDecimal calculatedTax = input.getCalculatedTax();
        BigDecimal withTaxePrice = input.getWithTaxePrice();

        // Then
        assertThat(calculatedTax.doubleValue()).isEqualTo(0);
        assertThat(withTaxePrice.doubleValue()).isEqualTo(12.49);
    }

    @Test
    public void should_return_cd_product_with_16_49_when_getWithTaxePrice_from_no_imported_cd_product_with_14_99() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.MUSIQUE)
                .imported(false)
                .price(new BigDecimal("14.99"))
                .build();

        // When
        BigDecimal calculatedTax = input.getCalculatedTax();
        BigDecimal withTaxePrice = input.getWithTaxePrice();

        // Then
        assertThat(calculatedTax.doubleValue()).isEqualTo(1.5);
        assertThat(withTaxePrice.doubleValue()).isEqualTo(16.49);
    }

    @Test
    public void should_return_perfume_product_with_32_19_when_getWithTaxePrice_from_imported_perfume_product_with_27_99() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.PARFUM)
                .imported(true)
                .price(new BigDecimal("27.99"))
                .build();

        // When
        BigDecimal calculatedTax = input.getCalculatedTax();
        BigDecimal withTaxePrice = input.getWithTaxePrice();

        // Then
        assertThat(calculatedTax.doubleValue()).isEqualTo(4.2);
        assertThat(withTaxePrice.doubleValue()).isEqualTo(32.19);
    }

    @Test
    public void should_return_chocolate_box_product_with_10_50_when_calculateTotalPrice_from_imported_chocolate_box_product_with_10_00() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.NOURRITURE)
                .imported(true)
                .price(new BigDecimal("10.00"))
                .build();

        // When
        BigDecimal calculatedTax = input.getCalculatedTax();
        BigDecimal withTaxePrice = input.getWithTaxePrice();

        // Then
        assertThat(calculatedTax.doubleValue()).isEqualTo(0.5);
        assertThat(withTaxePrice.doubleValue()).isEqualTo(10.50);
    }

    @Test
    public void should_return_imported_perfume_product_with_54_65_when_calculateTotalPrice_from_imported_perfume_product_with_47_50() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.PARFUM)
                .imported(true)
                .price(new BigDecimal("47.50"))
                .build();

        // When
        BigDecimal calculatedTax = input.getCalculatedTax();
        BigDecimal withTaxePrice = input.getWithTaxePrice();

        // Then
        assertThat(calculatedTax.doubleValue()).isEqualTo(7.15);
        assertThat(withTaxePrice.doubleValue()).isEqualTo(54.65);
    }

    @Test
    public void should_return_1_flacon_de_parfum_importé_when_toString_from_1_PARFUM_imported_32_19() {

        // Given
        Product input = Product.builder()
                .quantity(1)
                .productType(ProductType.PARFUM)
                .productTypeLabel("flacon de parfum")
                .imported(true)
                .importedLabel("importé")
                .price(new BigDecimal("32.19"))
                .build();

        // When
        String result = input.toString();

        // Then
        assertThat(result).isNotEmpty();
    }
}