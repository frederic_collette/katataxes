package caagis.kata.tax.model;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ProductTypeTest {

    @Test
    @Parameters({"livre,LIVRE",
            "boîte de pilules contre la migraine,MEDICAMENT",
            "CD musical,MUSIQUE",
            "barre de chocolat,NOURRITURE",
            "boîte de chocolats,NOURRITURE",
            "flacon de parfum,PARFUM"})
    public void should_return_known_product_category_when_fromLabel_from_known_label(final String aLabel, final ProductType aProductType) {
        // Given
        String input = aLabel;

        // When
        ProductType result = ProductType.fromLabel(input);

        //Then
        assertThat(result).isEqualTo(aProductType);
    }
}