package caagis.kata.tax.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;


public class BasketTest {

    @Test
    public void should_return_1_livre_12_49_when_printInvoice_from_1_livre_12_49_in_basket() {

        // Given
        Product product = Product.builder()
                .quantity(1)
                .productType(ProductType.LIVRE)
                .productTypeLabel("livre")
                .imported(false)
                .importedLabel("")
                .price(new BigDecimal("12.49"))
                .build();
        Basket input = new Basket(Collections.singletonList(product));

        // When
        String result = input.toString();

        // Then
        assertThat(result).isEqualTo("1 livre  : 12.49\nMontant des taxes : 0.00\nTotal : 12.49\n");
    }

    @Test
    public void should_return_2_elements_invoice_when_printInvoice_from_1_livre_12_49_and_in_basket() {

        // Given
        Product bookProduct = Product.builder()
                .quantity(1)
                .productType(ProductType.LIVRE)
                .productTypeLabel("livre")
                .importedLabel("")
                .price(new BigDecimal("12.49"))
                .build();

        Product importedChocolateBox = Product.builder()
                .quantity(1)
                .productType(ProductType.NOURRITURE)
                .productTypeLabel("boîte de chocolats")
                .imported(true)
                .importedLabel("importés")
                .price(new BigDecimal("10.00"))
                .build();

        Basket input = new Basket(Arrays.asList(bookProduct, importedChocolateBox));

        // When
        String result = input.toString();

        // Then
        assertThat(result).isEqualTo("1 livre  : 12.49\n1 boîte de chocolats importés : 10.50\nMontant des taxes : 0.50\nTotal : 22.99\n");
    }
}