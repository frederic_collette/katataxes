package caagis.kata.tax.parser;

import caagis.kata.tax.model.Basket;
import caagis.kata.tax.model.Product;
import caagis.kata.tax.model.ProductType;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class BasketParserTest {

    private BasketParser basketParser;

    @Before
    public void setup() {
        basketParser = new BasketParser();
    }

    @Test
    public void should_return_empty_basket_when_buildBasket_from_no_productLine() {

        // Given
        String input = "*  \n\r";

        // When
        Basket basket = basketParser.buildBasket(input);

        // Then
        assertThat(basket).isNotNull();
        assertThat(basket.getProducts()).isNotNull();
    }

    @Test
    public void should_return_3_product_book_cd_chocolate_bar_basket_when_buildBasket_from_3_productLines() {

        // Given
        String input = "* 1 livre à 12.49\n" +
                "* 1 CD musical à 14.99\n" +
                "* 1 barre de chocolat à 0.85";

        // When
        Basket basket = basketParser.buildBasket(input);

        // Then
        List<Product> result = basket.getProducts();
        assertThat(result).hasSize(3);

        Product bookProduct = result.get(0);
        assertThat(bookProduct.getQuantity()).isEqualTo(1);
        assertThat(bookProduct.getProductType()).isEqualTo(ProductType.LIVRE);
        assertThat(bookProduct.getProductTypeLabel()).isEqualTo("livre");
        assertThat(bookProduct.getPrice().doubleValue()).isEqualTo(12.49);
        assertThat(bookProduct.isImported()).isFalse();
        assertThat(bookProduct.getImportedLabel()).isEmpty();

        Product cdProduct = result.get(1);
        assertThat(cdProduct.getQuantity()).isEqualTo(1);
        assertThat(cdProduct.getProductType()).isEqualTo(ProductType.MUSIQUE);
        assertThat(cdProduct.getProductTypeLabel()).isEqualTo("CD musical");
        assertThat(cdProduct.getPrice().doubleValue()).isEqualTo(14.99);
        assertThat(cdProduct.isImported()).isFalse();
        assertThat(cdProduct.getImportedLabel()).isEmpty();

        Product chocolateBarProduct = result.get(2);
        assertThat(chocolateBarProduct.getQuantity()).isEqualTo(1);
        assertThat(chocolateBarProduct.getProductType()).isEqualTo(ProductType.NOURRITURE);
        assertThat(chocolateBarProduct.getProductTypeLabel()).isEqualTo("barre de chocolat");
        assertThat(chocolateBarProduct.getPrice().doubleValue()).isEqualTo(0.85);
        assertThat(chocolateBarProduct.isImported()).isFalse();
        assertThat(chocolateBarProduct.getImportedLabel()).isEmpty();
    }

    @Test
    public void should_return_2_imported_product_chocolate_perfume_basket_when_buildBasket_from_2_productLines() {

        // Given
        String input = "\n" +
                "* 1 boîte de chocolats importée à 10.00\n" +
                "* 1 flacon de parfum importé à 47.50";

        // When
        Basket basket = basketParser.buildBasket(input);

        // Then
        List<Product> result = basket.getProducts();
        assertThat(result).hasSize(2);

        Product chocolateBoxProduct = result.get(0);
        assertThat(chocolateBoxProduct.getQuantity()).isEqualTo(1);
        assertThat(chocolateBoxProduct.getProductType()).isEqualTo(ProductType.NOURRITURE);
        assertThat(chocolateBoxProduct.getProductTypeLabel()).isEqualTo("boîte de chocolats");
        assertThat(chocolateBoxProduct.getPrice().doubleValue()).isEqualTo(10.00);
        assertThat(chocolateBoxProduct.isImported()).isTrue();
        assertThat(chocolateBoxProduct.getImportedLabel()).isEqualTo("importée");

        Product perfumeProduct = result.get(1);
        assertThat(perfumeProduct.getQuantity()).isEqualTo(1);
        assertThat(perfumeProduct.getProductType()).isEqualTo(ProductType.PARFUM);
        assertThat(perfumeProduct.getProductTypeLabel()).isEqualTo("flacon de parfum");
        assertThat(perfumeProduct.getPrice().doubleValue()).isEqualTo(47.50);
        assertThat(perfumeProduct.isImported()).isTrue();
        assertThat(perfumeProduct.getImportedLabel()).isEqualTo("importé");
    }

    @Test
    public void should_return_4_imported_product_perfume_pills_chocoloate_basket_when_buildBasket_from_4_productLines() {

        // Given
        String input = "\n" +
                "* 1 flacon de parfum importé à 27.99\n" +
                "* 1 flacon de parfum à 18.99\n" +
                "* 1 boîte de pilules contre la migraine à 9.75\n" +
                "* 1 boîte de chocolats importés à 11.25";

        // When
        Basket basket = basketParser.buildBasket(input);

        // Then
        List<Product> result = basket.getProducts();
        assertThat(result).hasSize(4);

        Product importedPerfumeProduct = result.get(0);
        assertThat(importedPerfumeProduct.getQuantity()).isEqualTo(1);
        assertThat(importedPerfumeProduct.getProductType()).isEqualTo(ProductType.PARFUM);
        assertThat(importedPerfumeProduct.getProductTypeLabel()).isEqualTo("flacon de parfum");
        assertThat(importedPerfumeProduct.getPrice().doubleValue()).isEqualTo(27.99);
        assertThat(importedPerfumeProduct.isImported()).isTrue();
        assertThat(importedPerfumeProduct.getImportedLabel()).isEqualTo("importé");

        Product notImportedPerfumeProduct = result.get(1);
        assertThat(notImportedPerfumeProduct.getQuantity()).isEqualTo(1);
        assertThat(notImportedPerfumeProduct.getProductType()).isEqualTo(ProductType.PARFUM);
        assertThat(notImportedPerfumeProduct.getProductTypeLabel()).isEqualTo("flacon de parfum");
        assertThat(notImportedPerfumeProduct.getPrice().doubleValue()).isEqualTo(18.99);
        assertThat(notImportedPerfumeProduct.isImported()).isFalse();
        assertThat(notImportedPerfumeProduct.getImportedLabel()).isEmpty();

        Product medecinePillsProduct = result.get(2);
        assertThat(medecinePillsProduct.getQuantity()).isEqualTo(1);
        assertThat(medecinePillsProduct.getProductType()).isEqualTo(ProductType.MEDICAMENT);
        assertThat(medecinePillsProduct.getProductTypeLabel()).isEqualTo("boîte de pilules contre la migraine");
        assertThat(medecinePillsProduct.getPrice().doubleValue()).isEqualTo(9.75);
        assertThat(notImportedPerfumeProduct.isImported()).isFalse();
        assertThat(notImportedPerfumeProduct.getImportedLabel()).isEmpty();

        Product importedChocolateBoxProduct = result.get(3);
        assertThat(importedChocolateBoxProduct.getQuantity()).isEqualTo(1);
        assertThat(importedChocolateBoxProduct.getProductType()).isEqualTo(ProductType.NOURRITURE);
        assertThat(importedChocolateBoxProduct.getProductTypeLabel()).isEqualTo("boîte de chocolats");
        assertThat(importedChocolateBoxProduct.getPrice().doubleValue()).isEqualTo(11.25);
        assertThat(importedChocolateBoxProduct.isImported()).isTrue();
        assertThat(importedChocolateBoxProduct.getImportedLabel()).isEqualTo("importés");
    }
}