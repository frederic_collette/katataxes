package caagis.kata.tax;

import caagis.kata.tax.model.Basket;
import caagis.kata.tax.parser.BasketParser;
import caagis.kata.tax.parser.IBasketParser;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CommandMaker {

    private IBasketParser basketParser;

    private void init() {
        this.basketParser = new BasketParser();
    }

    private String generateInvoiceCommand(final String aCommand) {

        // Generate Basket from Command
        Basket result = basketParser.buildBasket(aCommand);

        // Generate invoice
        return result.toString();
    }

    public static void main(String... args) {

        CommandMaker commandMaker = new CommandMaker();
        commandMaker.init();

        String input1 = "* 1 livre à 12.49\n" +
                "* 1 CD musical à 14.99\n" +
                "* 1 barre de chocolat à 0.85";
        String input2 = "* 1 boîte de chocolats importée à 10.00\n" +
                "* 1 flacon de parfum importé à 47.50";
        String input3 = "* 1 flacon de parfum importé à 27.99\n" +
                "* 1 flacon de parfum à 18.99\n" +
                "* 1 boîte de pilules contre la migraine à 9.75\n" +
                "* 1 boîte de chocolats importés à 11.25";

        System.out.println(commandMaker.generateInvoiceCommand(input1));
        System.out.println(commandMaker.generateInvoiceCommand(input2));
        System.out.println(commandMaker.generateInvoiceCommand(input3));

    }
}
