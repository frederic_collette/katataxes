package caagis.kata.tax.parser;

import caagis.kata.tax.model.Basket;
import caagis.kata.tax.model.Product;
import caagis.kata.tax.model.ProductType;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@NoArgsConstructor
public class BasketParser implements IBasketParser {

    private static final String SEPARATOR_CHAR_REGEXP = "\\*";
    private static final String PRODUCT_REGEXP = "(\\d+)\\s(\\D+)\\sà\\s(.+)";
    private static final String IMPORTED_REGEXP = "importé";

    public Basket buildBasket(final String aBasketInput) {

        String[] productLines = aBasketInput.split(SEPARATOR_CHAR_REGEXP);
        ArrayList<Product> productBasket = new ArrayList<>();

        for (String productLine : productLines) {
            String essentialProductLine = productLine.replaceAll("[\\n\\r]", "").trim();
            if (!essentialProductLine.isEmpty()) {
                productBasket.add(buildProduct(essentialProductLine));
            }
        }

        return new Basket(productBasket);
    }

    private Product buildProduct(final String anInputLine) {

        Matcher matcher = Pattern.compile(PRODUCT_REGEXP).matcher(anInputLine);
        matcher.matches();

        int quantity = Integer.valueOf(matcher.group(1));

        String importedOrNotProductTypeLabel = matcher.group(2);
        String productTypeLabel = importedOrNotProductTypeLabel;
        String importedLabel = "";

        int importedIndex = importedOrNotProductTypeLabel.lastIndexOf(IMPORTED_REGEXP);
        boolean imported = importedIndex > -1;
        if (imported) {
            productTypeLabel = importedOrNotProductTypeLabel.substring(0, importedIndex).trim();
            importedLabel = importedOrNotProductTypeLabel.substring(importedIndex);
        }

        BigDecimal roundParsedPrice = new BigDecimal(matcher.group(3));

        return Product.builder().quantity(quantity).productType(ProductType.fromLabel(productTypeLabel)).productTypeLabel(productTypeLabel)
                .imported(imported).importedLabel(importedLabel).price(roundParsedPrice).build();
    }
}
