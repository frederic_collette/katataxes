package caagis.kata.tax.parser;


import caagis.kata.tax.model.Basket;

public interface IBasketParser {
    Basket buildBasket(final String aBasketInput);
}
