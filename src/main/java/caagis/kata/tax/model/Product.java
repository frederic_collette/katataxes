package caagis.kata.tax.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.math.RoundingMode;

@AllArgsConstructor
@Builder
@Getter
public class Product {

    private int quantity;
    private ProductType productType;
    private String productTypeLabel;
    private boolean imported;
    private String importedLabel;
    private BigDecimal price;


    BigDecimal getWithTaxePrice() {
        BigDecimal withoutTaxPrice = getWithoutTaxPrice();
        BigDecimal calculatedTaxToAdd = BigDecimal.valueOf(Tax.NONE.getAmount());

        if (this.productType.isHasProductTax()) {
            calculatedTaxToAdd = roundNearest5Cent(withoutTaxPrice.multiply(BigDecimal.valueOf(Tax.PRODUCT.getAmount())));
        }

        if (this.imported) {
            BigDecimal importedTaxPrice = roundNearest5Cent(withoutTaxPrice.multiply(BigDecimal.valueOf(Tax.IMPORTED.getAmount())));
            calculatedTaxToAdd = calculatedTaxToAdd.add(importedTaxPrice);
        }

        return withoutTaxPrice.add(calculatedTaxToAdd);
    }

    BigDecimal getCalculatedTax() {
        return getWithTaxePrice().subtract(getWithoutTaxPrice());
    }

    @Override
    public String toString() {
        return String.format("%s %s %s : %s\n", quantity, productTypeLabel, importedLabel, getWithTaxePrice());
    }

    private BigDecimal getWithoutTaxPrice() {
        return BigDecimal.valueOf(this.quantity).multiply(this.price);
    }

    private BigDecimal roundNearest5Cent(BigDecimal value) {
        BigDecimal nearest5Cent = new BigDecimal("0.05");
        BigDecimal divided = value.divide(nearest5Cent, 0, RoundingMode.UP);
        return divided.multiply(nearest5Cent);
    }
}
