package caagis.kata.tax.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
public class Basket {

    @Getter
    private List<Product> products;

    private String getBasketList() {
        return products.parallelStream().map(Product::toString).reduce("", String::concat);
    }

    private BigDecimal getTotalTax() {
        return products.parallelStream().map(Product::getCalculatedTax).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal getTotalPrice() {
        return products.parallelStream().map(Product::getWithTaxePrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public String toString() {
        return String.format(
                "%sMontant des taxes : %s\nTotal : %s\n",
                getBasketList(),
                getTotalTax(),
                getTotalPrice());
    }
}
