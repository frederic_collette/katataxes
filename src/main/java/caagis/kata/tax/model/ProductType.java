package caagis.kata.tax.model;


import lombok.Getter;

import java.util.HashMap;

public enum ProductType {

    LIVRE(false, "livre"),
    MEDICAMENT(false, "boîte de pilules contre la migraine"),
    MUSIQUE(true, "CD musical"),
    NOURRITURE(false, "barre de chocolat", "boîte de chocolats"),
    PARFUM(true, "flacon de parfum");

    @Getter
    private boolean hasProductTax;
    private String[] label;

    ProductType(boolean hasProductTax, String... aLabel) {
        this.hasProductTax = hasProductTax;
        this.label = aLabel;
    }

    private static HashMap<String, ProductType> fromLabelToProductTypeMap = new HashMap<>();

    static {
        for (ProductType pc : ProductType.values()) {

            fromLabelToProductTypeMap.put(pc.label[0], pc);
            if (pc.label.length > 1) {
                fromLabelToProductTypeMap.put(pc.label[1], pc);
            }
        }
    }

    public static ProductType fromLabel(final String aLabel) {
        return fromLabelToProductTypeMap.get(aLabel.trim());
    }
}
