package caagis.kata.tax.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Tax {

    NONE(0), IMPORTED(0.05), PRODUCT(0.1), BOTH(0.15);

    @Getter
    private double amount;
}